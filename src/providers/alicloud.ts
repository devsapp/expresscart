
import { spawn } from 'child_process';
import popCore from '@alicloud/pop-core';
import Logger from '../logger';
import CloudProvider from './provider';
import FC from './alicloud-fc';
const SERVERLESS_DEVS_NAMESPACE = 'serverlessdevs';
export default class AliCloud extends CloudProvider {
    protected client;
    protected namespace;
    constructor(props) {
        super(props);
        const { AccountID } = this.inputs.credentials;
        this.namespace = `${SERVERLESS_DEVS_NAMESPACE}${AccountID}`;
    }

    private async requestApi(path, method = 'GET', body = `{}`, option = {}) {
        const httpMethod = method;
        const uriPath = path;
        const queries = {};
        const headers = {
            "Content-Type": "application/json"
        };

        const apiClient = this.createApiClient();
        return await apiClient.request(httpMethod, uriPath, queries, body, headers, option);
    }
    private createApiClient(region = 'cn-hangzhou') {
        if (!this.client) {

            const { AccessKeyID, AccessKeySecret } = this.inputs.credentials;
            const _popCore = popCore as any;
            this.client = new _popCore.ROAClient({
                accessKeyId: AccessKeyID,
                accessKeySecret: AccessKeySecret,
                endpoint: `https://cr.${region}.aliyuncs.com`,
                apiVersion: '2016-06-07'
            });
        }
        return this.client;
    }

    private async getTempLoginUserInfo() {
        return this.requestApi('/tokens');
    }

    private executeLoginCommand(userName, password, region = 'cn-hangzhou') {
        return new Promise((resolve, reject) => {

            const buildCmd = spawn('docker', ['login', '-u', userName, '-p', password, `registry.${region}.aliyuncs.com`], { shell: true });
            buildCmd.stdout.on('data', (data) => {
                Logger.log(data.toString());
            });

            // buildCmd.stderr.on('data', (data) => {
            //     Logger.error(data.toString());
            //     reject(data);
            // });

            buildCmd.on('close', (code) => {
                Logger.success('login acr successfuly');
                resolve({ code });
            });
        });
    }

    private executeTagCommand(region: string, imgId: string): Promise<{ code: string | number, imgversion: string | number }> {
        const projectName = this.inputs.project.projectName.toLocaleLowerCase();
        const imgversion = Date.now();
        return new Promise((resolve, reject) => {
            const buildCmd = spawn('docker', ['tag', imgId, `registry.${region}.aliyuncs.com/${this.namespace}/${projectName}:${imgversion}`], { shell: true });
            buildCmd.stdout.on('data', (data) => {
                Logger.log(data.toString());
            });
            buildCmd.stderr.on('data', (data) => {
                Logger.error(data.toString());
                reject(data);
            });

            buildCmd.on('close', (code) => {
                Logger.success('docker tag successfuly');
                resolve({ code, imgversion });
            });
        })
    }

    private executePublishCommand(region: string, imgversion: string | number): Promise<{ code: string | number, imgversion: string | number }> {
        const projectName = this.inputs.project.projectName.toLocaleLowerCase();
        return new Promise((resolve, reject) => {
            const buildCmd = spawn('docker', ['push', `registry.${region}.aliyuncs.com/${this.namespace}/${projectName}:${imgversion}`], { shell: true });
            buildCmd.stdout.on('data', (data) => {
                Logger.log(data.toString());
            });
            buildCmd.stderr.on('data', (data) => {
                Logger.error(data.toString());
                reject(data);
            });

            buildCmd.on('close', (code) => {
                Logger.success('docker push execute successfuly');
                resolve({ code, imgversion });
            });
        })
    }

    private getNameSpace() {
        return this.requestApi('/namespace');
    }

    private async createNameSpace(name) {
        const body = {
            "Namespace": {
                "namespace": name
            }
        };
        return this.requestApi('/namespace', 'PUT', JSON.stringify(body));
    }

    private async updateNamespace(name) {
        const body = {
            "Namespace": {
                "AutoCreate": true,
                "DefaultVisibility": "PUBLIC"
            }
        }
        return this.requestApi(`/namespace/${name}`, 'POST', JSON.stringify(body));
    }

    private async createRepo(name) {
        const body = {
            "Repo": {
                "RepoNamespace": this.namespace,
                "RepoName": name,
                "Summary": "serverless devs",
                "Detail": "serverless devs",
                "RepoType": "PUBLIC"
            }
        }
        return this.requestApi('/repos', 'PUT', JSON.stringify(body));
    }

    private async getRepos() {
        return this.requestApi('/repos');
    }

    async login() {
        const tempLoginInfo = await this.getTempLoginUserInfo();
        if (tempLoginInfo.data) {
            const region = this.inputs.props.region || 'cn-hangzhou';
            const { tempUserName, authorizationToken } = tempLoginInfo.data;
            await this.executeLoginCommand(tempUserName, authorizationToken, region);
        }
    }

    async publish(buildImg: string): Promise<string> {
        const namespaceData = await this.getNameSpace();
        const { namespaces } = namespaceData.data;
        const namespaceNameList: any[] = namespaces.map(data => data.namespace);
        if (!namespaceNameList.includes(this.namespace)) {
            await this.createNameSpace(this.namespace).catch((e) => Logger.error(e));
        }
        await this.updateNamespace(this.namespace).catch((e) => Logger.error(e));
        const projectName = this.inputs.project.projectName.toLocaleLowerCase();
        const repoResult = await this.getRepos().catch((e) => Logger.error(e));
        const repos: any[] = repoResult.data.repos.map(item => item.repoName);
        if (!repos.includes(projectName)) {
            await this.createRepo(projectName).catch((e) => Logger.error(e));;
        }
        const region = this.inputs.props.region || 'cn-hangzhou';
        const { imgversion } = await this.executeTagCommand(region, buildImg);
        await this.executePublishCommand(region, imgversion);
        const imageUrl = `registry.${region}.aliyuncs.com/${this.namespace}/${projectName}:${imgversion}`;
        return imageUrl;

    }

    async deploy(imageRegistry:string) {
        const fc  = new FC(this.inputs);
        const domainResult = await fc.deploy(imageRegistry);
        return domainResult;
    }
}