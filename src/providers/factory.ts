import {InputProps} from '../entity';
import CloudProvider from './provider';
import AliCloud from './alicloud';
export default class ProviderFactory {

    static getProvider(inputs: InputProps): CloudProvider {
        return new AliCloud(inputs);
    }

}
