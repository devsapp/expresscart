import { InputProps } from '../entity';

export default abstract class CloudProvider {
    protected inputs: InputProps;

    constructor(inputs: InputProps) {
        this.inputs = inputs;
    }

    abstract login(); 
    abstract publish(buildImg:string):Promise<string>; 
    abstract deploy(imageRegistry:string);
 }
