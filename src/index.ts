
import path from 'path';
import { spawn } from 'child_process';
import BaseComponent from './base';
import logger from './logger';
import ProviderFactory from './providers/factory';
interface ICredentials {
  AccountID?: string,
  AccessKeyID?: string,
  AccessKeySecret?: string,
  SecretID?: string,
  SecretKey?: string,
  SecretAccessKey?: string,
  KeyVaultName?: string,
  TenantID?: string,
  ClientID?: string,
  ClientSecret?: string,
  PrivateKeyData?: string

}
interface InputProps {
  props: any, // 用户自定义输入
  credentials: ICredentials, // 用户秘钥
  appName: string, // 
  project: {
    component: string, // 组件名（支持本地绝对路径）
    access: string, // 访问秘钥名
    projectName: string, // 项目名
  },
  command: string, // 执行指令
  args: string, // 命令行 扩展参数
  path: {
    configPath: string // 配置路径
  }
}
export default class ComponentDemo extends BaseComponent {
  constructor(props) {
    super(props)
  }

  private async exeBuildImageCmd(packName: string, path: string): Promise<{ code: number, imgId: string }> {
    return new Promise((resolve, reject) => {
      const imgId = packName.toLocaleLowerCase();
      console.log(path, 'path');
      const buildCmd = spawn('docker', ['build', '-t', imgId, '.'], { shell: true, cwd: path, stdio: 'inherit' });
      // buildCmd.stdout.on('data', (data) => {
      //   logger.log(data.toString());
      // });

      // buildCmd.stderr.on('data', (data) => {
      //   logger.error(data.toString());
      //   reject(data);
      // });

      buildCmd.on('close', (code) => {
        logger.success('execute build pack successfuly');
        resolve({ code, imgId });
      });
    })
  }

  /**
   * 启动服务
   * @param inputs 
   */
  public async serve(inputs: InputProps) {
    const code = inputs.props.code;
    spawn('npm', ['start', inputs.args], { shell: true, cwd: path.join(process.cwd(), code), stdio: 'inherit' });
  }
  /**
   * 部署
   * @param inputs 
   */
  public async deploy(inputs: InputProps) {
    // 构建镜像
    const project = inputs.project;
    const inputParams = inputs.props;
    const projectName = project.projectName;
    const code = inputParams.code;

    const { imgId } = await this.exeBuildImageCmd(projectName, code);

    const provider = ProviderFactory.getProvider(inputs);
    await provider.login();
    const imageRegistry = await provider.publish(imgId);
    const result = await provider.deploy(imageRegistry);
    return result;
  }

  public async test(inputs: InputProps) {
    console.log(inputs);
  }
}
