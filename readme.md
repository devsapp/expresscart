## 说明
本项目是基于 [expressCart](https://github.com/mrvautin/expressCart) 开源购物网站的 组件，利用本组件可以快速完成一个自身电商购物网站的部署

您需要开通 [阿里云函数计算产品](https://www.aliyun.com/product/fc?spm=5176.10695662.1112509.1.70384357szNX9j) ,部署成功后需要设置一下 环境变量dbconnection=<your mongodb connectionUri> 即可通过生成的自定义域名进行访问



## 开发使用步骤

### 1 安装依赖
```
npm i 
```
### 2 执行编译
```
npm run build
```
### 3 执行测试


```
cd example && s serve
```
