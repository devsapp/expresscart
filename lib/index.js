"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var child_process_1 = require("child_process");
var base_1 = __importDefault(require("./base"));
var logger_1 = __importDefault(require("./logger"));
var factory_1 = __importDefault(require("./providers/factory"));
var ComponentDemo = /** @class */ (function (_super) {
    __extends(ComponentDemo, _super);
    function ComponentDemo(props) {
        return _super.call(this, props) || this;
    }
    ComponentDemo.prototype.exeBuildImageCmd = function (packName, path) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var imgId = packName.toLocaleLowerCase();
                        console.log(path, 'path');
                        var buildCmd = child_process_1.spawn('docker', ['build', '-t', imgId, '.'], { shell: true, cwd: path, stdio: 'inherit' });
                        // buildCmd.stdout.on('data', (data) => {
                        //   logger.log(data.toString());
                        // });
                        // buildCmd.stderr.on('data', (data) => {
                        //   logger.error(data.toString());
                        //   reject(data);
                        // });
                        buildCmd.on('close', function (code) {
                            logger_1.default.success('execute build pack successfuly');
                            resolve({ code: code, imgId: imgId });
                        });
                    })];
            });
        });
    };
    /**
     * 启动服务
     * @param inputs
     */
    ComponentDemo.prototype.serve = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var code;
            return __generator(this, function (_a) {
                code = inputs.props.code;
                child_process_1.spawn('npm', ['start', inputs.args], { shell: true, cwd: path_1.default.join(process.cwd(), code), stdio: 'inherit' });
                return [2 /*return*/];
            });
        });
    };
    /**
     * 部署
     * @param inputs
     */
    ComponentDemo.prototype.deploy = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var project, inputParams, projectName, code, imgId, provider, imageRegistry, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        project = inputs.project;
                        inputParams = inputs.props;
                        projectName = project.projectName;
                        code = inputParams.code;
                        return [4 /*yield*/, this.exeBuildImageCmd(projectName, code)];
                    case 1:
                        imgId = (_a.sent()).imgId;
                        provider = factory_1.default.getProvider(inputs);
                        return [4 /*yield*/, provider.login()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, provider.publish(imgId)];
                    case 3:
                        imageRegistry = _a.sent();
                        return [4 /*yield*/, provider.deploy(imageRegistry)];
                    case 4:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    ComponentDemo.prototype.test = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log(inputs);
                return [2 /*return*/];
            });
        });
    };
    return ComponentDemo;
}(base_1.default));
exports.default = ComponentDemo;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsOENBQXdCO0FBQ3hCLCtDQUFzQztBQUN0QyxnREFBbUM7QUFDbkMsb0RBQThCO0FBQzlCLGdFQUFrRDtBQThCbEQ7SUFBMkMsaUNBQWE7SUFDdEQsdUJBQVksS0FBSztlQUNmLGtCQUFNLEtBQUssQ0FBQztJQUNkLENBQUM7SUFFYSx3Q0FBZ0IsR0FBOUIsVUFBK0IsUUFBZ0IsRUFBRSxJQUFZOzs7Z0JBQzNELHNCQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07d0JBQ2pDLElBQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO3dCQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQzt3QkFDMUIsSUFBTSxRQUFRLEdBQUcscUJBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQzt3QkFDNUcseUNBQXlDO3dCQUN6QyxpQ0FBaUM7d0JBQ2pDLE1BQU07d0JBRU4seUNBQXlDO3dCQUN6QyxtQ0FBbUM7d0JBQ25DLGtCQUFrQjt3QkFDbEIsTUFBTTt3QkFFTixRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFDLElBQUk7NEJBQ3hCLGdCQUFNLENBQUMsT0FBTyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7NEJBQ2pELE9BQU8sQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUMsQ0FBQzt3QkFDM0IsQ0FBQyxDQUFDLENBQUM7b0JBQ0wsQ0FBQyxDQUFDLEVBQUE7OztLQUNIO0lBRUQ7OztPQUdHO0lBQ1UsNkJBQUssR0FBbEIsVUFBbUIsTUFBa0I7Ozs7Z0JBQzdCLElBQUksR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztnQkFDL0IscUJBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsY0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7Ozs7S0FDOUc7SUFDRDs7O09BR0c7SUFDVSw4QkFBTSxHQUFuQixVQUFvQixNQUFrQjs7Ozs7O3dCQUU5QixPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQzt3QkFDekIsV0FBVyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7d0JBQzNCLFdBQVcsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDO3dCQUNsQyxJQUFJLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQzt3QkFFWixxQkFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxFQUFBOzt3QkFBeEQsS0FBSyxHQUFLLENBQUEsU0FBOEMsQ0FBQSxNQUFuRDt3QkFFUCxRQUFRLEdBQUcsaUJBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3JELHFCQUFNLFFBQVEsQ0FBQyxLQUFLLEVBQUUsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7d0JBQ0QscUJBQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBQTs7d0JBQTdDLGFBQWEsR0FBRyxTQUE2Qjt3QkFDcEMscUJBQU0sUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQTdDLE1BQU0sR0FBRyxTQUFvQzt3QkFDbkQsc0JBQU8sTUFBTSxFQUFDOzs7O0tBQ2Y7SUFFWSw0QkFBSSxHQUFqQixVQUFrQixNQUFrQjs7O2dCQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7O0tBQ3JCO0lBQ0gsb0JBQUM7QUFBRCxDQUFDLEFBekRELENBQTJDLGNBQWEsR0F5RHZEIn0=