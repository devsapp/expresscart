import BaseComponent from './base';
interface ICredentials {
    AccountID?: string;
    AccessKeyID?: string;
    AccessKeySecret?: string;
    SecretID?: string;
    SecretKey?: string;
    SecretAccessKey?: string;
    KeyVaultName?: string;
    TenantID?: string;
    ClientID?: string;
    ClientSecret?: string;
    PrivateKeyData?: string;
}
interface InputProps {
    props: any;
    credentials: ICredentials;
    appName: string;
    project: {
        component: string;
        access: string;
        projectName: string;
    };
    command: string;
    args: string;
    path: {
        configPath: string;
    };
}
export default class ComponentDemo extends BaseComponent {
    constructor(props: any);
    private exeBuildImageCmd;
    /**
     * 启动服务
     * @param inputs
     */
    serve(inputs: InputProps): Promise<void>;
    /**
     * 部署
     * @param inputs
     */
    deploy(inputs: InputProps): Promise<any>;
    test(inputs: InputProps): Promise<void>;
}
export {};
