import { InputProps } from '../entity';
export default class FunctionCompute {
    protected inputs: InputProps;
    protected client: any;
    constructor(inputs: InputProps);
    private sendHttpRequest;
    private makeFcUtilsFunctionTmpDomainToken;
    private deleteFcUtilsFunctionTmpDomain;
    private processTemporaryDomain;
    createService(serviceName: string): Promise<string>;
    createOrUpdateFunction(serviceName: string, functionName: string, imageUrl: any): Promise<boolean>;
    /**ß
     * 创建or更新环境变量
     */
    createOrUpdateEnv(): Promise<void>;
    createTrigger(serviceName: string, functionName: string, triggerName: string): Promise<void>;
    private getCustomDomain;
    private updateCustomerDomain;
    private createCustomDomain;
    private listCustomDomains;
    checkDomainHasBindFunction(serviceName: any, functionName: any): Promise<any>;
    createOrUpdateCustomDomain(domainName: string, options: any): Promise<{}>;
    deploy(imageUrl: string): Promise<unknown>;
}
