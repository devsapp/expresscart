import { InputProps } from '../entity';
import CloudProvider from './provider';
export default class ProviderFactory {
    static getProvider(inputs: InputProps): CloudProvider;
}
