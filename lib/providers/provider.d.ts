import { InputProps } from '../entity';
export default abstract class CloudProvider {
    protected inputs: InputProps;
    constructor(inputs: InputProps);
    abstract login(): any;
    abstract publish(buildImg: string): Promise<string>;
    abstract deploy(imageRegistry: string): any;
}
