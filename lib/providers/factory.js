"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var alicloud_1 = __importDefault(require("./alicloud"));
var ProviderFactory = /** @class */ (function () {
    function ProviderFactory() {
    }
    ProviderFactory.getProvider = function (inputs) {
        return new alicloud_1.default(inputs);
    };
    return ProviderFactory;
}());
exports.default = ProviderFactory;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjdG9yeS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wcm92aWRlcnMvZmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUVBLHdEQUFrQztBQUNsQztJQUFBO0lBTUEsQ0FBQztJQUpVLDJCQUFXLEdBQWxCLFVBQW1CLE1BQWtCO1FBQ2pDLE9BQU8sSUFBSSxrQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFTCxzQkFBQztBQUFELENBQUMsQUFORCxJQU1DIn0=