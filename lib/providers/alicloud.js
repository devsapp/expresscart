"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
var pop_core_1 = __importDefault(require("@alicloud/pop-core"));
var logger_1 = __importDefault(require("../logger"));
var provider_1 = __importDefault(require("./provider"));
var alicloud_fc_1 = __importDefault(require("./alicloud-fc"));
var SERVERLESS_DEVS_NAMESPACE = 'serverlessdevs';
var AliCloud = /** @class */ (function (_super) {
    __extends(AliCloud, _super);
    function AliCloud(props) {
        var _this = _super.call(this, props) || this;
        var AccountID = _this.inputs.credentials.AccountID;
        _this.namespace = "" + SERVERLESS_DEVS_NAMESPACE + AccountID;
        return _this;
    }
    AliCloud.prototype.requestApi = function (path, method, body, option) {
        if (method === void 0) { method = 'GET'; }
        if (body === void 0) { body = "{}"; }
        if (option === void 0) { option = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var httpMethod, uriPath, queries, headers, apiClient;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        httpMethod = method;
                        uriPath = path;
                        queries = {};
                        headers = {
                            "Content-Type": "application/json"
                        };
                        apiClient = this.createApiClient();
                        return [4 /*yield*/, apiClient.request(httpMethod, uriPath, queries, body, headers, option)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AliCloud.prototype.createApiClient = function (region) {
        if (region === void 0) { region = 'cn-hangzhou'; }
        if (!this.client) {
            var _a = this.inputs.credentials, AccessKeyID = _a.AccessKeyID, AccessKeySecret = _a.AccessKeySecret;
            var _popCore = pop_core_1.default;
            this.client = new _popCore.ROAClient({
                accessKeyId: AccessKeyID,
                accessKeySecret: AccessKeySecret,
                endpoint: "https://cr." + region + ".aliyuncs.com",
                apiVersion: '2016-06-07'
            });
        }
        return this.client;
    };
    AliCloud.prototype.getTempLoginUserInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.requestApi('/tokens')];
            });
        });
    };
    AliCloud.prototype.executeLoginCommand = function (userName, password, region) {
        if (region === void 0) { region = 'cn-hangzhou'; }
        return new Promise(function (resolve, reject) {
            var buildCmd = child_process_1.spawn('docker', ['login', '-u', userName, '-p', password, "registry." + region + ".aliyuncs.com"], { shell: true });
            buildCmd.stdout.on('data', function (data) {
                logger_1.default.log(data.toString());
            });
            // buildCmd.stderr.on('data', (data) => {
            //     Logger.error(data.toString());
            //     reject(data);
            // });
            buildCmd.on('close', function (code) {
                logger_1.default.success('login acr successfuly');
                resolve({ code: code });
            });
        });
    };
    AliCloud.prototype.executeTagCommand = function (region, imgId) {
        var _this = this;
        var projectName = this.inputs.project.projectName.toLocaleLowerCase();
        var imgversion = Date.now();
        return new Promise(function (resolve, reject) {
            var buildCmd = child_process_1.spawn('docker', ['tag', imgId, "registry." + region + ".aliyuncs.com/" + _this.namespace + "/" + projectName + ":" + imgversion], { shell: true });
            buildCmd.stdout.on('data', function (data) {
                logger_1.default.log(data.toString());
            });
            buildCmd.stderr.on('data', function (data) {
                logger_1.default.error(data.toString());
                reject(data);
            });
            buildCmd.on('close', function (code) {
                logger_1.default.success('docker tag successfuly');
                resolve({ code: code, imgversion: imgversion });
            });
        });
    };
    AliCloud.prototype.executePublishCommand = function (region, imgversion) {
        var _this = this;
        var projectName = this.inputs.project.projectName.toLocaleLowerCase();
        return new Promise(function (resolve, reject) {
            var buildCmd = child_process_1.spawn('docker', ['push', "registry." + region + ".aliyuncs.com/" + _this.namespace + "/" + projectName + ":" + imgversion], { shell: true });
            buildCmd.stdout.on('data', function (data) {
                logger_1.default.log(data.toString());
            });
            buildCmd.stderr.on('data', function (data) {
                logger_1.default.error(data.toString());
                reject(data);
            });
            buildCmd.on('close', function (code) {
                logger_1.default.success('docker push execute successfuly');
                resolve({ code: code, imgversion: imgversion });
            });
        });
    };
    AliCloud.prototype.getNameSpace = function () {
        return this.requestApi('/namespace');
    };
    AliCloud.prototype.createNameSpace = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var body;
            return __generator(this, function (_a) {
                body = {
                    "Namespace": {
                        "namespace": name
                    }
                };
                return [2 /*return*/, this.requestApi('/namespace', 'PUT', JSON.stringify(body))];
            });
        });
    };
    AliCloud.prototype.updateNamespace = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var body;
            return __generator(this, function (_a) {
                body = {
                    "Namespace": {
                        "AutoCreate": true,
                        "DefaultVisibility": "PUBLIC"
                    }
                };
                return [2 /*return*/, this.requestApi("/namespace/" + name, 'POST', JSON.stringify(body))];
            });
        });
    };
    AliCloud.prototype.createRepo = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var body;
            return __generator(this, function (_a) {
                body = {
                    "Repo": {
                        "RepoNamespace": this.namespace,
                        "RepoName": name,
                        "Summary": "serverless devs",
                        "Detail": "serverless devs",
                        "RepoType": "PUBLIC"
                    }
                };
                return [2 /*return*/, this.requestApi('/repos', 'PUT', JSON.stringify(body))];
            });
        });
    };
    AliCloud.prototype.getRepos = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.requestApi('/repos')];
            });
        });
    };
    AliCloud.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var tempLoginInfo, region, _a, tempUserName, authorizationToken;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.getTempLoginUserInfo()];
                    case 1:
                        tempLoginInfo = _b.sent();
                        if (!tempLoginInfo.data) return [3 /*break*/, 3];
                        region = this.inputs.props.region || 'cn-hangzhou';
                        _a = tempLoginInfo.data, tempUserName = _a.tempUserName, authorizationToken = _a.authorizationToken;
                        return [4 /*yield*/, this.executeLoginCommand(tempUserName, authorizationToken, region)];
                    case 2:
                        _b.sent();
                        _b.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AliCloud.prototype.publish = function (buildImg) {
        return __awaiter(this, void 0, void 0, function () {
            var namespaceData, namespaces, namespaceNameList, projectName, repoResult, repos, region, imgversion, imageUrl;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getNameSpace()];
                    case 1:
                        namespaceData = _a.sent();
                        namespaces = namespaceData.data.namespaces;
                        namespaceNameList = namespaces.map(function (data) { return data.namespace; });
                        if (!!namespaceNameList.includes(this.namespace)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.createNameSpace(this.namespace).catch(function (e) { return logger_1.default.error(e); })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [4 /*yield*/, this.updateNamespace(this.namespace).catch(function (e) { return logger_1.default.error(e); })];
                    case 4:
                        _a.sent();
                        projectName = this.inputs.project.projectName.toLocaleLowerCase();
                        return [4 /*yield*/, this.getRepos().catch(function (e) { return logger_1.default.error(e); })];
                    case 5:
                        repoResult = _a.sent();
                        repos = repoResult.data.repos.map(function (item) { return item.repoName; });
                        if (!!repos.includes(projectName)) return [3 /*break*/, 7];
                        return [4 /*yield*/, this.createRepo(projectName).catch(function (e) { return logger_1.default.error(e); })];
                    case 6:
                        _a.sent();
                        ;
                        _a.label = 7;
                    case 7:
                        region = this.inputs.props.region || 'cn-hangzhou';
                        return [4 /*yield*/, this.executeTagCommand(region, buildImg)];
                    case 8:
                        imgversion = (_a.sent()).imgversion;
                        return [4 /*yield*/, this.executePublishCommand(region, imgversion)];
                    case 9:
                        _a.sent();
                        imageUrl = "registry." + region + ".aliyuncs.com/" + this.namespace + "/" + projectName + ":" + imgversion;
                        return [2 /*return*/, imageUrl];
                }
            });
        });
    };
    AliCloud.prototype.deploy = function (imageRegistry) {
        return __awaiter(this, void 0, void 0, function () {
            var fc, domainResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fc = new alicloud_fc_1.default(this.inputs);
                        return [4 /*yield*/, fc.deploy(imageRegistry)];
                    case 1:
                        domainResult = _a.sent();
                        return [2 /*return*/, domainResult];
                }
            });
        });
    };
    return AliCloud;
}(provider_1.default));
exports.default = AliCloud;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxpY2xvdWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvcHJvdmlkZXJzL2FsaWNsb3VkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLCtDQUFzQztBQUN0QyxnRUFBeUM7QUFDekMscURBQStCO0FBQy9CLHdEQUF1QztBQUN2Qyw4REFBK0I7QUFDL0IsSUFBTSx5QkFBeUIsR0FBRyxnQkFBZ0IsQ0FBQztBQUNuRDtJQUFzQyw0QkFBYTtJQUcvQyxrQkFBWSxLQUFLO1FBQWpCLFlBQ0ksa0JBQU0sS0FBSyxDQUFDLFNBR2Y7UUFGVyxJQUFBLFNBQVMsR0FBSyxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsVUFBNUIsQ0FBNkI7UUFDOUMsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFHLHlCQUF5QixHQUFHLFNBQVcsQ0FBQzs7SUFDaEUsQ0FBQztJQUVhLDZCQUFVLEdBQXhCLFVBQXlCLElBQUksRUFBRSxNQUFjLEVBQUUsSUFBVyxFQUFFLE1BQVc7UUFBeEMsdUJBQUEsRUFBQSxjQUFjO1FBQUUscUJBQUEsRUFBQSxXQUFXO1FBQUUsdUJBQUEsRUFBQSxXQUFXOzs7Ozs7d0JBQzdELFVBQVUsR0FBRyxNQUFNLENBQUM7d0JBQ3BCLE9BQU8sR0FBRyxJQUFJLENBQUM7d0JBQ2YsT0FBTyxHQUFHLEVBQUUsQ0FBQzt3QkFDYixPQUFPLEdBQUc7NEJBQ1osY0FBYyxFQUFFLGtCQUFrQjt5QkFDckMsQ0FBQzt3QkFFSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO3dCQUNsQyxxQkFBTSxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLEVBQUE7NEJBQW5GLHNCQUFPLFNBQTRFLEVBQUM7Ozs7S0FDdkY7SUFDTyxrQ0FBZSxHQUF2QixVQUF3QixNQUFzQjtRQUF0Qix1QkFBQSxFQUFBLHNCQUFzQjtRQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUVSLElBQUEsS0FBbUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQXhELFdBQVcsaUJBQUEsRUFBRSxlQUFlLHFCQUE0QixDQUFDO1lBQ2pFLElBQU0sUUFBUSxHQUFHLGtCQUFjLENBQUM7WUFDaEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ2pDLFdBQVcsRUFBRSxXQUFXO2dCQUN4QixlQUFlLEVBQUUsZUFBZTtnQkFDaEMsUUFBUSxFQUFFLGdCQUFjLE1BQU0sa0JBQWU7Z0JBQzdDLFVBQVUsRUFBRSxZQUFZO2FBQzNCLENBQUMsQ0FBQztTQUNOO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFYSx1Q0FBb0IsR0FBbEM7OztnQkFDSSxzQkFBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFDOzs7S0FDckM7SUFFTyxzQ0FBbUIsR0FBM0IsVUFBNEIsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFzQjtRQUF0Qix1QkFBQSxFQUFBLHNCQUFzQjtRQUNsRSxPQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFL0IsSUFBTSxRQUFRLEdBQUcscUJBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLGNBQVksTUFBTSxrQkFBZSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUNoSSxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBQyxJQUFJO2dCQUM1QixnQkFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztZQUVILHlDQUF5QztZQUN6QyxxQ0FBcUM7WUFDckMsb0JBQW9CO1lBQ3BCLE1BQU07WUFFTixRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFDLElBQUk7Z0JBQ3RCLGdCQUFNLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLG9DQUFpQixHQUF6QixVQUEwQixNQUFjLEVBQUUsS0FBYTtRQUF2RCxpQkFrQkM7UUFqQkcsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDeEUsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQzlCLE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixJQUFNLFFBQVEsR0FBRyxxQkFBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsY0FBWSxNQUFNLHNCQUFpQixLQUFJLENBQUMsU0FBUyxTQUFJLFdBQVcsU0FBSSxVQUFZLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ3BKLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFDLElBQUk7Z0JBQzVCLGdCQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLFVBQUMsSUFBSTtnQkFDNUIsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqQixDQUFDLENBQUMsQ0FBQztZQUVILFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUMsSUFBSTtnQkFDdEIsZ0JBQU0sQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDekMsT0FBTyxDQUFDLEVBQUUsSUFBSSxNQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8sd0NBQXFCLEdBQTdCLFVBQThCLE1BQWMsRUFBRSxVQUEyQjtRQUF6RSxpQkFpQkM7UUFoQkcsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDeEUsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9CLElBQU0sUUFBUSxHQUFHLHFCQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsTUFBTSxFQUFFLGNBQVksTUFBTSxzQkFBaUIsS0FBSSxDQUFDLFNBQVMsU0FBSSxXQUFXLFNBQUksVUFBWSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUM5SSxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBQyxJQUFJO2dCQUM1QixnQkFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztZQUNILFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFDLElBQUk7Z0JBQzVCLGdCQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2dCQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakIsQ0FBQyxDQUFDLENBQUM7WUFFSCxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFDLElBQUk7Z0JBQ3RCLGdCQUFNLENBQUMsT0FBTyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7Z0JBQ2xELE9BQU8sQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLFVBQVUsWUFBQSxFQUFFLENBQUMsQ0FBQztZQUNsQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVPLCtCQUFZLEdBQXBCO1FBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFYSxrQ0FBZSxHQUE3QixVQUE4QixJQUFJOzs7O2dCQUN4QixJQUFJLEdBQUc7b0JBQ1QsV0FBVyxFQUFFO3dCQUNULFdBQVcsRUFBRSxJQUFJO3FCQUNwQjtpQkFDSixDQUFDO2dCQUNGLHNCQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUM7OztLQUNyRTtJQUVhLGtDQUFlLEdBQTdCLFVBQThCLElBQUk7Ozs7Z0JBQ3hCLElBQUksR0FBRztvQkFDVCxXQUFXLEVBQUU7d0JBQ1QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLG1CQUFtQixFQUFFLFFBQVE7cUJBQ2hDO2lCQUNKLENBQUE7Z0JBQ0Qsc0JBQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBYyxJQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBQzs7O0tBQzlFO0lBRWEsNkJBQVUsR0FBeEIsVUFBeUIsSUFBSTs7OztnQkFDbkIsSUFBSSxHQUFHO29CQUNULE1BQU0sRUFBRTt3QkFDSixlQUFlLEVBQUUsSUFBSSxDQUFDLFNBQVM7d0JBQy9CLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixTQUFTLEVBQUUsaUJBQWlCO3dCQUM1QixRQUFRLEVBQUUsaUJBQWlCO3dCQUMzQixVQUFVLEVBQUUsUUFBUTtxQkFDdkI7aUJBQ0osQ0FBQTtnQkFDRCxzQkFBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFDOzs7S0FDakU7SUFFYSwyQkFBUSxHQUF0Qjs7O2dCQUNJLHNCQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUM7OztLQUNwQztJQUVLLHdCQUFLLEdBQVg7Ozs7OzRCQUMwQixxQkFBTSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBQTs7d0JBQWpELGFBQWEsR0FBRyxTQUFpQzs2QkFDbkQsYUFBYSxDQUFDLElBQUksRUFBbEIsd0JBQWtCO3dCQUNaLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDO3dCQUNuRCxLQUF1QyxhQUFhLENBQUMsSUFBSSxFQUF2RCxZQUFZLGtCQUFBLEVBQUUsa0JBQWtCLHdCQUFBLENBQXdCO3dCQUNoRSxxQkFBTSxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLGtCQUFrQixFQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBeEUsU0FBd0UsQ0FBQzs7Ozs7O0tBRWhGO0lBRUssMEJBQU8sR0FBYixVQUFjLFFBQWdCOzs7Ozs0QkFDSixxQkFBTSxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUE7O3dCQUF6QyxhQUFhLEdBQUcsU0FBeUI7d0JBQ3ZDLFVBQVUsR0FBSyxhQUFhLENBQUMsSUFBSSxXQUF2QixDQUF3Qjt3QkFDcEMsaUJBQWlCLEdBQVUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxTQUFTLEVBQWQsQ0FBYyxDQUFDLENBQUM7NkJBQ3BFLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBM0Msd0JBQTJDO3dCQUMzQyxxQkFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxnQkFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBZixDQUFlLENBQUMsRUFBQTs7d0JBQXhFLFNBQXdFLENBQUM7OzRCQUU3RSxxQkFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxnQkFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBZixDQUFlLENBQUMsRUFBQTs7d0JBQXhFLFNBQXdFLENBQUM7d0JBQ25FLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzt3QkFDckQscUJBQU0sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLGdCQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFmLENBQWUsQ0FBQyxFQUFBOzt3QkFBaEUsVUFBVSxHQUFHLFNBQW1EO3dCQUNoRSxLQUFLLEdBQVUsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFFBQVEsRUFBYixDQUFhLENBQUMsQ0FBQzs2QkFDbEUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUE1Qix3QkFBNEI7d0JBQzVCLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQWYsQ0FBZSxDQUFDLEVBQUE7O3dCQUFoRSxTQUFnRSxDQUFDO3dCQUFBLENBQUM7Ozt3QkFFaEUsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUM7d0JBQ2xDLHFCQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLEVBQUE7O3dCQUE3RCxVQUFVLEdBQUssQ0FBQSxTQUE4QyxDQUFBLFdBQW5EO3dCQUNsQixxQkFBTSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxFQUFBOzt3QkFBcEQsU0FBb0QsQ0FBQzt3QkFDL0MsUUFBUSxHQUFHLGNBQVksTUFBTSxzQkFBaUIsSUFBSSxDQUFDLFNBQVMsU0FBSSxXQUFXLFNBQUksVUFBWSxDQUFDO3dCQUNsRyxzQkFBTyxRQUFRLEVBQUM7Ozs7S0FFbkI7SUFFSyx5QkFBTSxHQUFaLFVBQWEsYUFBb0I7Ozs7Ozt3QkFDdkIsRUFBRSxHQUFJLElBQUkscUJBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ1gscUJBQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQTdDLFlBQVksR0FBRyxTQUE4Qjt3QkFDbkQsc0JBQU8sWUFBWSxFQUFDOzs7O0tBQ3ZCO0lBQ0wsZUFBQztBQUFELENBQUMsQUE5S0QsQ0FBc0Msa0JBQWEsR0E4S2xEIn0=